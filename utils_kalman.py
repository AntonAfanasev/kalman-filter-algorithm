import numpy as np
import matplotlib.pyplot as plt
matplotlib.use('PDF')

def generate_true_trajectory_cartesian(N, T, v_0, q_0):
    
    x = np.zeros(N)
    y = np.zeros(N)
    x[0], y[0] = q_0
    v = v_0

    for i in xrange(1, N):
        x[i] = x[i-1] + v[0]*T
        y[i] = y[i-1] + v[1]*T
        
    return x, y

def cartesian_to_polar(x, y):
    
    return np.hypot(x, y), np.arctan2(x, y)

def polar_to_cartesian(D, beta):
    
    return D * np.sin(beta), D * np.cos(beta)

def generate_measurements_polar(D, beta, sigma_D, sigma_beta):
    
    N = len(D)
    return D + sigma_D * np.random.randn(N), beta + sigma_beta * np.random.randn(N)
	
def generate_measurements_azimuth(beta, sigma_beta_acc):
    
    N = len(beta)
    return beta + sigma_beta_acc * np.random.randn(N)

def transport_matrix_joint(T):
    
    F = np.array([[1, T, 0, 0], [0, 1, 0, 0], [0 , 0, 1, T], [0, 0, 0, 1]])
    F_tr = np.transpose(F)
    return F, F_tr

def transport_matrix_independent(T):
    
    F = np.array([[1, T], [0, 1]])
    F_tr = np.transpose(F)
    return F, F_tr
	
def transport_matrix_extended(T):
    
    F = np.array([[1, T, 0, 0], [0, 1, 0, 0], [0 , 0, 1, T], [0, 0, 0, 1]])
    F_tr = np.transpose(F)
    return F, F_tr
	
def observation_matrix_joint():
    
    H = np.array([[1, 0, 0, 0], [0, 0, 1, 0]])
    H_tr = np.transpose(H)
    return H, H_tr

def observation_matrix_independent():
    
    H = np.array([1, 0])
    H_tr = np.array([[1], [0]])
    return H, H_tr

def observation_matrix_extended(x_pred, y_pred):
    
    d2_pred = x_pred**2 + y_pred**2
    d_pred = np.sqrt(d2_pred)
    H = np.zeros((2, 4)).astype(np.float32)
    H[0,0] = 1. * x_pred / d_pred
    H[0,2] = 1. * y_pred / d_pred
    H[1,0] = 1. * y_pred / d2_pred
    H[1,2] = -1. * x_pred / d2_pred
    H_tr = np.transpose(H)
    return H, H_tr
	
def observation_matrix_extended_acc(x_pred, y_pred):
    
    d2_pred = x_pred**2 + y_pred**2
    H = np.zeros((1, 4)).astype(np.float32)
    H[0, 0] = 1. * y_pred / d2_pred
    H[0, 2] = -1. * x_pred / d2_pred
    H_tr = np.transpose(H)
    return H[0], H_tr
	
def error_covariance_matrix_joint(D_m, beta_m, sigma_D, sigma_beta):
    
    N = len(D_m)
    R = np.zeros((2, 2, N))
    R[0,0,:] = np.power(sigma_D * np.sin(beta_m), 2) + np.power(sigma_beta * np.cos(beta_m) * D_m, 2)
    R[1,1,:] = np.power(sigma_D * np.cos(beta_m), 2) + np.power(sigma_beta * np.sin(beta_m) * D_m, 2)
    R[0,1,:] = R[1,0,:] = np.sin(beta_m) * np.cos(beta_m) * (sigma_D**2 - np.power(sigma_beta * D_m, 2))
    return R

def error_covariance_matrix_independent(sigma_D):
    
    return sigma_D**2

def error_covariance_matrix_extended(sigma_D, sigma_beta):
    
    return np.array([[sigma_D**2, 0], [0, sigma_beta**2]])

def error_covariance_matrix_extended_acc(sigma_beta):
    
    return sigma_beta**2
	
def kalman_filtration_joint(x_m, y_m, F, F_tr, H, H_tr, R, X_0, P_0):
    
    N = len(x_m)
    Z_m = np.dstack((x_m, y_m))
    Z_m = np.transpose(Z_m, axes=(2,0,1))
    
    # state vector
    X_fil = np.zeros((4, 1, N))
    X_pred = np.zeros((4, 1, N))
    X_fil[:,:,0] = X_pred[:,:,0] = X_0
    
    # filtration error covariance matrix
    P_fil = np.zeros((4, 4, N))
    P_pred = np.zeros((4, 4, N))
    P_fil[:,:,0] = P_pred[:,:,0] = P_0
    
    # Kalman gain
    K = np.zeros((4, 2, N))
    
    for i in xrange(1, N):
        X_pred[:,:,i] = np.dot(F, X_fil[:,:,i-1])
        P_pred[:,:,i] = np.dot(np.dot(F, P_fil[:,:,i-1]), F_tr)
        K[:,:,i] = np.dot(np.dot(P_pred[:,:,i], H_tr), np.linalg.inv(np.dot(np.dot(H, P_pred[:,:,i]), H_tr) + R[:,:,i]))
        P_fil[:,:,i] = np.dot((np.identity(4) - np.dot(K[:,:,i], H)), P_pred[:,:,i])
        X_fil[:,:,i] = X_pred[:,:,i] + np.dot(K[:,:,i], (Z_m[:,:,i] - np.dot(H, X_pred[:,:,i]))) 
        
    return X_fil, X_pred

def kalman_filtration_independent(x_m, y_m, F, F_tr, H, H_tr, R, X_0, Y_0, P_0):
    
    N = len(x_m)
    
    # state vector X
    X_fil = np.zeros((2, 1, N))
    X_pred = np.zeros((2, 1, N))
    X_fil[:,:,0] = X_pred[:,:,0] = X_0
    
    # state vector Y
    Y_fil = np.zeros((2, 1, N))
    Y_pred = np.zeros((2, 1, N))
    Y_fil[:,:,0] = Y_pred[:,:,0] = Y_0
    
    # filtration error covariance matrix
    P_fil = np.zeros((2, 2, N))
    P_pred = np.zeros((2, 2, N))
    P_fil[:,:,0] = P_pred[:,:,0] = P_0
    
    # Kalman gain
    K = np.zeros((2, 1, N))
    
    for i in xrange(1, N):
        X_pred[:,:,i] = np.dot(F, X_fil[:,:,i-1])
        Y_pred[:,:,i] = np.dot(F, Y_fil[:,:,i-1])
        P_pred[:,:,i] = np.dot(np.dot(F, P_fil[:,:,i-1]), F_tr)
        K[:,:,i] = np.dot(P_pred[:,:,i], H_tr) / (np.dot(np.dot(H, P_pred[:,:,i]), H_tr) + R)
        P_fil[:,:,i] = np.dot((np.identity(2) - np.multiply(K[:,:,i], H)), P_pred[:,:,i])
        X_fil[:,:,i] = X_pred[:,:,i] + np.multiply(K[:,:,i], (x_m[i] - np.dot(H, X_pred[:,:,i])))
        Y_fil[:,:,i] = Y_pred[:,:,i] + np.multiply(K[:,:,i], (y_m[i] - np.dot(H, Y_pred[:,:,i])))
        
    return X_fil, Y_fil, X_pred, Y_pred

def kalman_filtration_extended(D_m, beta_m, F, F_tr, R, X_0, P_0):
    
    N = len(D_m)
    Z_m = np.dstack((D_m, beta_m))
    Z_m = np.transpose(Z_m, axes=(2,0,1))
    
    # state vector
    X_fil = np.zeros((4, 1, N))
    X_pred = np.zeros((4, 1, N))
    X_fil[:,:,0] = X_pred[:,:,0] = X_0
    
    # filtration error covariance matrix
    P_fil = np.zeros((4, 4, N))
    P_pred = np.zeros((4, 4, N))
    P_fil[:,:,0] = P_pred[:,:,0] = P_0
    
    # Kalman gain
    K = np.zeros((4, 2, N))
    
    for i in xrange(1, N):
        X_pred[:,:,i] = np.dot(F, X_fil[:,:,i-1])
        P_pred[:,:,i] = np.dot(np.dot(F, P_fil[:,:,i-1]), F_tr)
        H, H_tr = observation_matrix_extended(X_pred[0,0,i], X_pred[2,0,i])
        K[:,:,i] = np.dot(np.dot(P_pred[:,:,i], H_tr), np.linalg.inv(np.dot(np.dot(H, P_pred[:,:,i]), H_tr) + R))
        P_fil[:,:,i] = np.dot((np.identity(4) - np.dot(K[:,:,i], H)), P_pred[:,:,i])
        D_pred, beta_pred = cartesian_to_polar(X_pred[0,0,i], X_pred[2,0,i])
        X_fil[:,:,i] = X_pred[:,:,i] + np.dot(K[:,:,i], (Z_m[:,:,i] - np.array([[D_pred], [beta_pred]])))
        
    return X_fil, X_pred
	
def kalman_filtration_unified(z1, z2, F, F_tr, R, R_acc, X_0, P_0):
    
    N = len(z2)
    z1 = np.dstack((z1[0,:], z1[1,:]))
    z1 = np.transpose(z1, axes=(2,0,1))
    
    # state vector
    X_fil = np.zeros((4, 1, N))
    X_pred = np.zeros((4, 1, N))
    X_fil[:,:,2] = X_pred[:,:,2] = X_0
    
    # filtration error covariance matrix
    P_fil = np.zeros((4, 4, N))
    P_pred = np.zeros((4, 4, N))
    P_fil[:,:,2] = P_pred[:,:,2] = P_0
    
    # Kalman gain
    K = np.zeros((4, 2, N))
    K_acc = np.zeros((4, 1, N))
    
    for i in xrange(3, N):
        X_pred[:,:,i] = np.dot(F, X_fil[:,:,i-1])
        D_pred, beta_pred = cartesian_to_polar(X_pred[0,0,i], X_pred[2,0,i])
        P_pred[:,:,i] = np.dot(np.dot(F, P_fil[:,:,i-1]), F_tr)
        if (i%2 == 0):
            H, H_tr = observation_matrix_extended(X_pred[0,0,i], X_pred[2,0,i])
            K[:,:,i] = np.dot(np.dot(P_pred[:,:,i], H_tr), np.linalg.inv(np.dot(np.dot(H, P_pred[:,:,i]), H_tr) + R))
            P_fil[:,:,i] = np.dot((np.identity(4) - np.dot(K[:,:,i], H)), P_pred[:,:,i])
            X_fil[:,:,i] = X_pred[:,:,i] + np.dot(K[:,:,i], (z1[:,:,i] - np.array([[D_pred], [beta_pred]])))
        else:
            H, H_tr = observation_matrix_extended_acc(X_pred[0,0,i], X_pred[2,0,i])
            K_acc[:,:,i] = np.dot(P_pred[:,:,i], H_tr) / (np.dot(np.dot(H, P_pred[:,:,i]), H_tr) + R_acc)
            P_fil[:,:,i] = np.dot((np.identity(4) - np.multiply(K_acc[:,:,i], H)), P_pred[:,:,i])
            X_fil[:,:,i] = X_pred[:,:,i] + np.dot(K_acc[:,:,i], (z2[i] - beta_pred))
        
    return X_fil, X_pred
	
def true_estimation_error_joint(M, D, beta, sigma_D, sigma_beta, F, F_tr, H, H_tr, X_0, P_0):

    N = len(D)
    
    error_D_fil = np.zeros(N)
    error_beta_fil = np.zeros(N)
    error_D_pred = np.zeros(N)
    error_beta_pred = np.zeros(N)

    for i in xrange(M):

        D_m, beta_m = generate_measurements_polar(D, beta, sigma_D, sigma_beta)
        x_m, y_m = polar_to_cartesian(D_m, beta_m)
        R = error_covariance_matrix_joint(D_m, beta_m, sigma_D, sigma_beta)
        X_fil, X_pred = kalman_filtration_joint(x_m, y_m, F, F_tr, H, H_tr, R, X_0, P_0)
        
        D_fil, beta_fil = cartesian_to_polar(X_fil[0,0,:], X_fil[2,0,:])
        error_D_fil += np.power(D_fil - D, 2)
        error_beta_fil += np.power(beta_fil - beta, 2)
        
        D_pred, beta_pred = cartesian_to_polar(X_pred[0,0,:], X_pred[2,0,:])
        error_D_pred += np.power(D_pred - D, 2)
        error_beta_pred += np.power(beta_pred - beta, 2)

    final_error_D_fil = np.sqrt(error_D_fil / (M-1))
    final_error_beta_fil = np.sqrt(error_beta_fil / (M-1))
    
    final_error_D_pred = np.sqrt(error_D_pred / (M-1))
    final_error_beta_pred = np.sqrt(error_beta_pred / (M-1))
    
    return final_error_D_fil, final_error_beta_fil, final_error_D_pred, final_error_beta_pred

def true_estimation_error_independent(M, D, beta, sigma_D, sigma_beta, F, F_tr, H, H_tr, X_0, Y_0, P_0):

    N = len(D)
    
    error_D_fil = np.zeros(N)
    error_beta_fil = np.zeros(N)
    error_D_pred = np.zeros(N)
    error_beta_pred = np.zeros(N)
    
    R = error_covariance_matrix_independent(sigma_D)

    for i in xrange(M):

        D_m, beta_m = generate_measurements_polar(D, beta, sigma_D, sigma_beta)
        x_m, y_m = polar_to_cartesian(D_m, beta_m)
        X_fil, Y_fil, X_pred, Y_pred = kalman_filtration_independent(x_m, y_m, F, F_tr, H, H_tr, R, X_0, Y_0, P_0)
        
        D_fil, beta_fil = cartesian_to_polar(X_fil[0,0,:], Y_fil[0,0,:])
        error_D_fil += np.power(D_fil - D, 2)
        error_beta_fil += np.power(beta_fil - beta, 2)
        
        D_pred, beta_pred = cartesian_to_polar(X_pred[0,0,:], Y_pred[0,0,:])
        error_D_pred += np.power(D_pred - D, 2)
        error_beta_pred += np.power(beta_pred - beta, 2)

    final_error_D_fil = np.sqrt(error_D_fil / (M-1))
    final_error_beta_fil = np.sqrt(error_beta_fil / (M-1))
    
    final_error_D_pred = np.sqrt(error_D_pred / (M-1))
    final_error_beta_pred = np.sqrt(error_beta_pred / (M-1))
    
    return final_error_D_fil, final_error_beta_fil, final_error_D_pred, final_error_beta_pred

def true_estimation_error_extended(M, D, beta, sigma_D, sigma_beta, F, F_tr, P_0):

    N = len(D)
    
    error_D_fil = np.zeros(N)
    error_beta_fil = np.zeros(N)
    error_D_pred = np.zeros(N)
    error_beta_pred = np.zeros(N)

    R = error_covariance_matrix_extended(sigma_D, sigma_beta)
    
    for i in xrange(M):

        D_m, beta_m = generate_measurements_polar(D, beta, sigma_D, sigma_beta)
        x_est, y_est = polar_to_cartesian(D_m[0], beta_m[0])
        X_0 = [[x_est], [0], [y_est], [0]]
        
        X_fil, X_pred = kalman_filtration_extended(D_m, beta_m, F, F_tr, R, X_0, P_0)
        
        D_fil, beta_fil = cartesian_to_polar(X_fil[0,0,:], X_fil[2,0,:])
        error_D_fil += np.power(D_fil - D, 2)
        error_beta_fil += np.power(beta_fil - beta, 2)
        
        D_pred, beta_pred = cartesian_to_polar(X_pred[0,0,:], X_pred[2,0,:])
        error_D_pred += np.power(D_pred - D, 2)
        error_beta_pred += np.power(beta_pred - beta, 2)

    final_error_D_fil = np.sqrt(error_D_fil / (M-1))
    final_error_beta_fil = np.sqrt(error_beta_fil / (M-1))
    
    final_error_D_pred = np.sqrt(error_D_pred / (M-1))
    final_error_beta_pred = np.sqrt(error_beta_pred / (M-1))
    
    return final_error_D_fil, final_error_beta_fil, final_error_D_pred, final_error_beta_pred
	
def true_estimation_error_unified(M, D, beta, sigma_D, sigma_beta, sigma_beta_acc, F, F_tr, P_0):

    N = len(D)
    
    error_D_fil = np.zeros(N)
    error_beta_fil = np.zeros(N)
    error_D_pred = np.zeros(N)
    error_beta_pred = np.zeros(N)

    R = error_covariance_matrix_extended(sigma_D, sigma_beta)
    R_acc = error_covariance_matrix_extended_acc(sigma_beta_acc)
    
    for i in xrange(M):

        z1 = generate_measurements_polar(D, beta, sigma_D, sigma_beta)
        z1_log = np.tile(np.array([1,0]), int(N/2))
        z1 = z1 * z1_log

        z2 = generate_measurements_azimuth(beta, sigma_beta_acc)
        z2_log = np.tile(np.array([0,1]), int(N/2))
        z2_log[1] = 0
        z2 = z2 * z2_log
        
        (x0_m, y0_m) = polar_to_cartesian(z1[0,0], z1[1,0])
        (x2_m, y2_m) = polar_to_cartesian(z1[0,2], z1[1,2])
        X_0 = [[x2_m], [(x2_m - x0_m) / (2. * T)], [y2_m], [(y2_m - y0_m) / (2. * T)]]
        
        X_fil, X_pred = kalman_filtration_unified(z1, z2, F, F_tr, R, R_acc, X_0, P_0)
        
        D_fil, beta_fil = cartesian_to_polar(X_fil[0,0,:], X_fil[2,0,:])
        error_D_fil += np.power(D_fil - D, 2)
        error_beta_fil += np.power(beta_fil - beta, 2)
        
        D_pred, beta_pred = cartesian_to_polar(X_pred[0,0,:], X_pred[2,0,:])
        error_D_pred += np.power(D_pred - D, 2)
        error_beta_pred += np.power(beta_pred - beta, 2)

    final_error_D_fil = np.sqrt(error_D_fil / (M-1))
    final_error_beta_fil = np.sqrt(error_beta_fil / (M-1))
    
    final_error_D_pred = np.sqrt(error_D_pred / (M-1))
    final_error_beta_pred = np.sqrt(error_beta_pred / (M-1))
    
    return final_error_D_fil, final_error_beta_fil, final_error_D_pred, final_error_beta_pred
	
#-----------------------------------------------------------------------------------------------------------------

def range_or_iter_ax(ax, range_ax):
    
    if range_ax:
        ax.set_xlabel('$Range, m$', fontsize=16)
        ax.invert_xaxis()
    else:
        ax.set_xlabel('$Iteration$', fontsize=16)

#-------------------------------------------------------------------------------------------------------------------------------------------

### --------------------------------------- Comment to method "compare_errs" ------------------------------------------ ###
# M - amount of cycles in estimation of filtration and extrapolation errors
# N - amount of processed points for moving objects
# T - sampling interval, s
# v_0 = (vx0, vy0) - initial velocity, (m/s, m/s)
# q_0 = (x0, y0) - initial coordinates, (m, m)
# Traverse - traverse range, m
# sigma_D - standard deviation of range noise, m
# sigma_beta - standard deviation of azimuth noise, rad
# sigma_beta_acc - lesser standard deviation of azimuth noise, rad
# Z_0 = (x0_pred, vx0_pred, y0_pred, vy0_pred) - initial approximation of state-vector, (m, m/s, m, m/s); (40000,-20,40000,-20) by default
# P_00 - initial diagonal element of error covariance matrix; 1e+10 by default
# rel_err - flag, which indicates if relative (rel_err=True) or absolute (rel_err=False) estimation errors will be shown; True by default
# joint - flag, which indicates if errors of joint filter will be shown (joint=True) or not (joint=False); True by default
# ind - flag, which indicates if errors of independent filter will be shown (ind=True) or not (ind=False); True by default
# extend - flag, which indicates if errors of extended filter will be shown (extend=True) or not (extend=False); True by default
# unif - flag, which indicates if errors of unified filter will be shown (unif=True) or not (unif=False); True by default
# extrap - flag, which indicates if errors of extrapolated estimates will be shown (extrap=True) or not (extrap=False); True by default
# extrap_start - number of iteration, starting with which errors of extrapolated estimates will be shown; 3 by default
# fil_start - number of iteration, starting with which errors of filtered estimates will be shown; 1 by default
# range_ax - flag, which indicates if range (range_ax=True) or iterations (range_ax=False) will be shown on X axis; True by default
# traj - flag, which indicates if trajectory of moving object will be shown (traj=True) or not (traj=False); True by default
# c_num - flag, which indicates if condition number of measurement error covariance matrix will be shown (c_num=True) 
#         or not (c_num=False); True by default
# x_beta - flag, which indicates if dependency of x coordinate on azimuth will be shown (x_beta=True) or not (x_beta=False); True by default
def compare_errs(M, N, T, v_0, q_0, Traverse, sigma_D, sigma_beta, sigma_beta_acc, Z_0=(40000,-20,40000,-20), P_00=1e+10,
                 rel_err=True, joint=True, ind=True, extend=True, unif=True, extrap=True, extrap_start=3, fil_start=1, 
                 range_ax=True, traj=True, c_num=True, x_beta=True):
	
	# even number of time samples for unified filter
	if (unif and N%2 != 0):
		N = N + 1
	
	# central line - bisector of the first quadrant
	if traj:
		x_c, y_c = generate_true_trajectory_cartesian(N, T, v_0, q_0)
	
	# initial coordinate of the moving object located on the traverse distance from the bisector
	q_0_traversed = np.add(q_0, (Traverse/np.sqrt(2), -Traverse/np.sqrt(2)))
	
	# components of initial approximation for state-vector X (x-coord, x-velocity, y-coord, y-velocity)
	x0, vx0, y0, vy0 = Z_0
	
	# trajectory of the moving object in cartesian coordinates
	x, y = generate_true_trajectory_cartesian(N, T, v_0, q_0_traversed)
	
	# trajectory of the moving object in polar coordinates
	D, beta = cartesian_to_polar(x, y)
	
	# condition number and measurements for plot
	if traj or c_num:
		D_m, beta_m = generate_measurements_polar(D, beta, sigma_D, sigma_beta)
	if traj:
		x_m, y_m = polar_to_cartesian(D_m, beta_m)
	if c_num:
		lam_div = np.power(D_m, 2)*(sigma_beta/sigma_D)**2
		cond_num = lam_div * (lam_div >= 1) + 1./lam_div * (1./lam_div > 1)
	
	# initial approximation of state-vector and error covariance matrix for filter with joint coordinates and extended one
	X_0 = [[x0], [vx0], [y0], [vy0]]
	P_0 = [[P_00, 0, 0, 0], [0, P_00, 0, 0], [0, 0, P_00, 0], [0, 0, 0, P_00]]
	
	# transport and observation matrices for filter with joint coordinates
	F, F_tr = transport_matrix_joint(T)
	H, H_tr = observation_matrix_joint()
	
	# true estimation errors of range and azimuth for filter with joint coordinates
	e_D_f_j, e_beta_f_j, e_D_p_j, e_beta_p_j = true_estimation_error_joint(M, D, beta, sigma_D, sigma_beta, F, F_tr, H, H_tr, X_0, P_0)
	
	# transport matrix for extended filter
	F, F_tr = transport_matrix_extended(T)
	
	# true estimation errors of range and azimuth for extended filter
	e_D_f_ext, e_beta_f_ext, e_D_p_ext, e_beta_p_ext = true_estimation_error_extended(M, D, beta, sigma_D, sigma_beta, F, F_tr, P_0)
  	  
	# true estimation errors of range and azimuth for unified filter
	e_D_f_un, e_beta_f_un, e_D_p_un, e_beta_p_un = true_estimation_error_unified(M, D, beta, sigma_D, sigma_beta, sigma_beta_acc, F, F_tr, P_0)
	
	# initial approximation of state-vectors and error covariance matrix for filter with independent coordinates
	X_0 = [[x0], [vx0]]
	Y_0 = [[y0], [vy0]]
	P_0 = [[P_00, 0], [0, P_00]]
	
	# transport and observation matrices for filter with independent coordinates
	F, F_tr = transport_matrix_independent(T)
	H, H_tr = observation_matrix_independent()
	
	# results of kalman filtration with independent coordinates for plot
	if traj:
		R = error_covariance_matrix_independent(sigma_D)
		X_fil, Y_fil, X_pred, Y_pred = kalman_filtration_independent(x_m, y_m, F, F_tr, H, H_tr, R, X_0, Y_0, P_0)
	
	# true estimation errors of range and azimuth for filter with independent coordinates
	e_D_f_ind, e_beta_f_ind, e_D_p_ind, e_beta_p_ind = true_estimation_error_independent(M, D, beta, sigma_D, sigma_beta, F, F_tr, H, H_tr, 
                                                                                         X_0, Y_0, P_0)
	
	# relative estimation errors of range and azimuth filtered estimates for all filters (j/ind/ext/un = joint/independent/extended/unified)
	e_D_f_j_rel = e_D_f_j / sigma_D
	e_D_f_ind_rel = e_D_f_ind / sigma_D
	e_D_f_ext_rel = e_D_f_ext / sigma_D
	e_D_f_un_rel = e_D_f_un / sigma_D
	e_beta_f_j_rel = e_beta_f_j / sigma_beta
	e_beta_f_ind_rel = e_beta_f_ind / sigma_beta
	e_beta_f_ext_rel = e_beta_f_ext / sigma_beta
	e_beta_f_un_rel = e_beta_f_un / sigma_beta
	# relative estimation errors of range and azimuth extrapolated estimates for all filters (j/ind/ext/un = joint/independent/extended/unified)
	e_D_p_j_rel = e_D_p_j / sigma_D
	e_D_p_ind_rel = e_D_p_ind / sigma_D
	e_D_p_ext_rel = e_D_p_ext / sigma_D
	e_D_p_un_rel = e_D_p_un / sigma_D
	e_beta_p_j_rel = e_beta_p_j / sigma_beta
	e_beta_p_ind_rel = e_beta_p_ind / sigma_beta
	e_beta_p_ext_rel = e_beta_p_ext / sigma_beta
	e_beta_p_un_rel = e_beta_p_un / sigma_beta
	
	# plotting trajectory of the moving object (being plotted if "traj" flag is raised)
	if traj:
		f1, ax1 = plt.subplots(1)
		ax1.plot(x_c, y_c, linestyle='--', color='r', label = 'Central line')
		ax1.plot(x, y, label = 'Traverse trajectory, $Traverse=%5.1f[m],x_0=%5.1f[m],v^x_0=%5.1f[m/s],\\Delta T=%5.1i[s]$'
                 % (Traverse, q_0[0], v_0[0], T))
		ax1.plot(x_m, y_m, color='y', label = 'Measurements, $\\sigma_D={}[m],\\sigma_\\beta={}[rad]$'
                 .format(sigma_D, sigma_beta))
		ax1.plot(X_fil[0,0,fil_start:], Y_fil[0,0,fil_start:], color='g', 
                 label = 'Filtered estimates, $x^{pred}_0=%5.1f[m],v^{x,pred}_0=%5.1f[m/s],P^{00}_0=%1.1E$'
                 % (x0, vx0, P_00))
		ax1.plot(0, 0, 'or')
		ax1.axis('equal')
		ax1.set_ylabel('$y$', fontsize=20)
		ax1.set_xlabel('$x$', fontsize=20)
		ax1.set_title('Traverse trajectory', fontsize=16)
		ax1.grid(True)
		lgd = ax1.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
		plt.savefig('traj' + '.pdf', format='pdf', bbox_extra_artists=(lgd,), bbox_inches='tight')
	
	# plotting estimation errors
	# choosing the form of X axis (range/iterations) depending on the "range_ax" flag
	if range_ax:
		x_axis_fil = D[fil_start:]
		x_axis_pred = D[extrap_start:]
		x_axis_cnum = D
	else:
		x_axis_fil = np.arange(fil_start, len(D))
		x_axis_pred = np.arange(extrap_start, len(D))
		x_axis_cnum = np.arange(len(D))
	
	# plotting condition number of measurement error covariance matrix, xlabel is set in "range_or_iter_ax" method
	# number of subplots and figsize are also dependent on "c_num" flag (3/2; 12x9/12x6)
	if c_num:
		f2, ax2 = plt.subplots(3, sharex=True, figsize=(12,9))
		ax2[2].plot(x_axis_cnum, cond_num)
		ax2[2].set_title('Condition number of error covariance matrix', fontsize=18)
		range_or_iter_ax(ax2[2], range_ax)
	else:
		f2, ax2 = plt.subplots(2, sharex=True, figsize=(12,6))
		range_or_iter_ax(ax2[1], range_ax)
	
	# choosing the form of errors of filtered estimates (relative/absolute) depending on the "rel_err" flag
	if rel_err:
		if joint:
			ax2[0].plot(x_axis_fil, e_D_f_j_rel[fil_start:], label='Filtered\nJoint')
			ax2[1].plot(x_axis_fil, e_beta_f_j_rel[fil_start:], label='Filtered\nJoint')
		if ind:
			ax2[0].plot(x_axis_fil, e_D_f_ind_rel[fil_start:], label='Filtered\nIndependent')
			ax2[1].plot(x_axis_fil, e_beta_f_ind_rel[fil_start:], label='Filtered\nIndependent')
		if extend:
			ax2[0].plot(x_axis_fil, e_D_f_ext_rel[fil_start:], label='Filtered\nExtended')
			ax2[1].plot(x_axis_fil, e_beta_f_ext_rel[fil_start:], label='Filtered\nExtended')
		if unif:
			ax2[0].plot(x_axis_fil, e_D_f_un_rel[fil_start:], label='Filtered\nUnified')
			ax2[1].plot(x_axis_fil, e_beta_f_un_rel[fil_start:], label='Filtered\nUnified')
		ax2[0].set_title('Relative estimation error of range', fontsize=18)
		ax2[0].set_ylabel('$err_D / \\sigma_D$', fontsize=16)
		ax2[1].set_title('Relative estimation error of azimuth', fontsize=18)
		ax2[1].set_ylabel('$err_{\\beta} / \\sigma_{\\beta}$', fontsize=16)
		if extrap:
			if joint:
				ax2[0].plot(x_axis_pred, e_D_p_j_rel[extrap_start:], label='Extrapolated\nJoint')
				ax2[1].plot(x_axis_pred, e_beta_p_j_rel[extrap_start:], label='Extrapolated\nJoint')
			if ind:
				ax2[0].plot(x_axis_pred, e_D_p_ind_rel[extrap_start:], label='Extrapolated\nIndependent')
				ax2[1].plot(x_axis_pred, e_beta_p_ind_rel[extrap_start:], label='Extrapolated\nIndependent')
			if extend:
				ax2[0].plot(x_axis_pred, e_D_p_ext_rel[extrap_start:], label='Extrapolated\nExtended')
				ax2[1].plot(x_axis_pred, e_beta_p_ext_rel[extrap_start:], label='Extrapolated\nExtended')
			if unif:
				ax2[0].plot(x_axis_pred, e_D_p_un_rel[extrap_start:], label='Extrapolated\nUnified')
				ax2[1].plot(x_axis_pred, e_beta_p_un_rel[extrap_start:], label='Extrapolated\nUnified')
	else:
		if joint:
			ax2[0].plot(x_axis_fil, e_D_f_j[fil_start:], label='Filtered\nJoint')
			ax2[1].plot(x_axis_fil, e_beta_f_j[fil_start:], label='Filtered\nJoint')
		if ind:
			ax2[0].plot(x_axis_fil, e_D_f_ind[fil_start:], label='Filtered\nIndependent')
			ax2[1].plot(x_axis_fil, e_beta_f_ind[fil_start1:], label='Filtered\nIndependent')
		if extend:
			ax2[0].plot(x_axis_fil, e_D_f_ext[fil_start:], label='Filtered\nExtended')
			ax2[1].plot(x_axis_fil, e_beta_f_ext[fil_start1:], label='Filtered\nExtended')
		if unif:
			ax2[0].plot(x_axis_fil, e_D_f_un[fil_start:], label='Filtered\nUnified')
			ax2[1].plot(x_axis_fil, e_beta_f_un[fil_start1:], label='Filtered\nUnified')
		ax2[0].set_title('True estimation error of range', fontsize=18)
		ax2[0].set_ylabel('$err_D, m$', fontsize=16)
		ax2[1].set_title('True estimation error of azimuth', fontsize=18)
		ax2[1].set_ylabel('$err_{\\beta}, rad$', fontsize=16)
		if extrap:
			if joint:
				ax2[0].plot(x_axis_pred, e_D_p_j[extrap_start:], label='Extrapolated\nJoint')
				ax2[1].plot(x_axis_pred, e_beta_p_j[extrap_start:], label='Extrapolated\nJoint')
			if ind:
				ax2[0].plot(x_axis_pred, e_D_p_ind[extrap_start:], label='Extrapolated\nIndependent')
				ax2[1].plot(x_axis_pred, e_beta_p_ind[extrap_start:], label='Extrapolated\nIndependent')
			if extend:
				ax2[0].plot(x_axis_pred, e_D_p_ext[extrap_start:], label='Extrapolated\nExtended')
				ax2[1].plot(x_axis_pred, e_beta_p_ext[extrap_start:], label='Extrapolated\nExtended')
			if unif:
				ax2[0].plot(x_axis_pred, e_D_p_un[extrap_start:], label='Extrapolated\nUnified')
				ax2[1].plot(x_axis_pred, e_beta_p_un[extrap_start:], label='Extrapolated\nUnified')
	lgd1 = ax2[0].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=10)
	lgd2 = ax2[1].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=10)
	plt.savefig('errs' + '.pdf', format='pdf', bbox_extra_artists=(lgd1,lgd2,), bbox_inches='tight')
	
	# plotting dependency of x coordinate on azimuth (being plotted if "x_beta" flag is raised)
	if x_beta:
		f3, ax3 = plt.subplots(1)
		ax3.plot(beta, x)
		ax3.set_ylabel('$x$', fontsize=20)
		ax3.set_xlabel('$\\beta$', fontsize=20)
		ax3.set_title('Dependency of $x$ on $\\beta$', fontsize=18)
		plt.savefig('x_beta' + '.pdf', format='pdf')